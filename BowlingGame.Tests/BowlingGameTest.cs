using System;
using Xunit;

namespace BowlingGame.Tests
{
    public class BowlingGameTest
    {
        [Fact]
        public void TestBowlingGameCalculationsForZeroScore()
        {
            BowlingGameCalculator bowlingGame = new BowlingGameCalculator();
            int[] rolls = new int[21];
            RollMultiple(bowlingGame, rolls);
            Assert.Equal(0, bowlingGame.Score());
        }

        [Fact]
        public void TestBowlingGameCalculationsWithoutBonus()
        {
            BowlingGameCalculator bowlingGame = new BowlingGameCalculator();
            int[] rolls = new int[21] {1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,5,3,0};
            RollMultiple(bowlingGame, rolls);
            Assert.Equal(35, bowlingGame.Score());
        }

        [Fact]
        public void TestBowlingGameCalculationsWithSpare()
        {
            BowlingGameCalculator bowlingGame = new BowlingGameCalculator();
            int[] rolls = new int[21] {1, 9, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 5, 3, 0 };
            RollMultiple(bowlingGame, rolls);
            Assert.Equal(43, bowlingGame.Score());
        }

        [Fact]
        public void TestBowlingGameCalculationsWithSpike()
        {
            BowlingGameCalculator bowlingGame = new BowlingGameCalculator();
            int[] rolls = new int[19] {10, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 5, 3};
            RollMultiple(bowlingGame, rolls);
            Assert.Equal(45, bowlingGame.Score());
        }

        [Fact]
        public void TestBowlingGameCalculationsWithSpikeAndSpare()
        {
            BowlingGameCalculator bowlingGame = new BowlingGameCalculator();
            int[] rolls = new int[12] {10, 10, 10, 10, 10, 10, 10, 10, 10, 5, 5, 10};
            RollMultiple(bowlingGame, rolls);
            Assert.Equal(275, bowlingGame.Score());
        }

        [Fact]
        public void TestBowlingGameCalculationsForPerfectGame()
        {
            BowlingGameCalculator bowlingGame = new BowlingGameCalculator();
            int[] rolls = new int[12] { 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10};
            RollMultiple(bowlingGame, rolls);
            Assert.Equal(300, bowlingGame.Score());
        }

        private void RollMultiple(BowlingGameCalculator bowlingGame, Array rolls)
        {
            foreach (int roll in rolls)
            {
                bowlingGame.Roll(roll);
            }
        }
    }
}
