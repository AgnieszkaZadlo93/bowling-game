﻿using System.Collections.Generic;

namespace BowlingGame
{
    public class BowlingGameCalculator
    {
        private List<int> rolls = new List<int>(21);
        public void Roll(int pins)
        {
            rolls.Add(pins);
        }

        public int Score()
        {
            int frame;
            int roll = 0;
            int sum = 0;

            for (frame = 0; frame < 10; frame++)
            {
                if (IsSpare(roll))
                {
                    sum += CountPointsWithBonus(roll);
                    roll += 2;
                }
                else if (IsSpike(roll))
                {
                    sum += CountPointsWithBonus(roll);
                    roll += 1;
                }
                else
                {
                    sum +=  rolls[roll] + rolls[roll + 1];
                    roll += 2;
                }
            }

            return sum;
        }

        private bool IsSpare(int roll)
        {
            return rolls[roll] + rolls[roll + 1] == 10;
        }

        private bool IsSpike(int roll)
        {
            return rolls[roll] == 10;
        }

        private int CountPointsWithBonus(int roll)
        {
            return rolls[roll] + rolls[roll + 1] + rolls[roll + 2];
        }
    }
}
